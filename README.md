# Lapce Ansible

Plugin for Lapce that aims to implement similar functionality to the [Ansible plugin for VSCodium](https://open-vsx.org/extension/redhat/ansible) (sans telemetry).

# Building

(You probably know this if you have used Rust before)

Make sure you're using `rustup` instead of your distro's packaged `rust`.

On Arch Linux:

```bash
sudo pacman -S rustup
rustup default stable
rustup target add wasm32-wasi
```

After that you should be able to run `cargo build` without any issues. If you don't, you get a gazillion errors.

# Requirements

- ansible-language-server installed (https://als.readthedocs.io/)
- ansible-lint installed (https://ansible-lint.readthedocs.io/installing/)
